package eu.transkribus.core.model.beans;

public class TrpUsageStats {
	Integer loggedIn;
	Integer registered;

	public TrpUsageStats() {}
	
	public Integer getLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(Integer loggedIn) {
		this.loggedIn = loggedIn;
	}
	public Integer getRegistered() {
		return registered;
	}
	public void setRegistered(Integer registered) {
		this.registered = registered;
	}

	@Override
	public String toString() {
		return "TrpUsageStats [loggedIn=" + loggedIn + ", registered=" + registered + "]";
	}
}
